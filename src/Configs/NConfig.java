package Configs;

import java.io.IOException;
import java.util.*;
import java.util.function.BiConsumer;

/**
 * A new configuration type. Using bitmagic in this one now.
 * Created by linus on 25.03.2016.
 */
public class NConfig {
    private int[] rows; //Two bits represent a tile: 00 = unknown, 01 = empty, 10 = atom , 11 = error
    private int atomCount;

    /**
     * Creates an empty configuration.
     */
    public NConfig() {
        atomCount = 0;
        rows = new int[8];
        rows[0] = rows[7] = 0x5555;
        for (int i = 1; i < 7; i++) {
            rows[i] = 0B01_00_00_00_00_00_00_01;
        }
    }


    /**
     * Creates an configuration based on the given rows. Can be inefficient, because it calculates the amount of atoms
     * 'by hand'.
     *
     * @param rows An array of length 8 representing the rows of the configuration.
     */
    private NConfig(int[] rows) {
        this.rows = rows;
        atomCount = 0;
        for (int i = 1; i < 7; i++) {
            int tmp = rows[i];
            while (tmp > 0) {
                if ((tmp & 0B11) == 0B10) {
                    atomCount++;
                }
                tmp = tmp >>> 2;
            }
        }
    }

    /**
     * Creates an configuration based on the given rows. This is more efficient, because it does not have to count the
     * amount of atoms in the configuration.
     *
     * @param rows      An array of length 8 representing the rows of the configuration.
     * @param atomCount The amount of atoms inside the given configuration.
     */
    private NConfig(int[] rows, int atomCount) {
        this.rows = rows;
        this.atomCount = atomCount;
    }

    static List<List<Integer[]>> readInput() {
        List<List<Integer[]>> userInputs = new ArrayList<>();
        int amountConf = readNumber("\\n");
        for (int i = 0; i < amountConf; i++) {
            readNumber("\\n");
            int amountOfExperiments = readNumber("\\n");
            List<Integer[]> setOfExperiments = new ArrayList<>();
            for (int j = 0; j < amountOfExperiments; j++) {
                setOfExperiments.add(new Integer[]{readNumber(" "), readNumber("\\n")});
            }
            userInputs.add(setOfExperiments);
        }
        return userInputs;
    }

    private static int readNumber(String terminatedBy) {
        String input = "";
        try {
            while (!input.matches("-??\\d*" + terminatedBy)) {
                input += (char) System.in.read();
            }
        } catch (IOException e) {
            return -1;
        }
        if (input.length() < 2)
            return 0;
        if (input.matches("-??\\d*\\s"))
            return Integer.parseInt(input.substring(0, input.length() - 1));
        return Integer.parseInt(input);
    }

    public static void main(String[] args) {
        List<List<Integer[]>> userInputs = readInput();
        for (int i = 0; i < 1; i++) {
            long startTime = System.currentTimeMillis();
            NConfig clearField = new NConfig();
            String output = "";
            boolean secondExperiment = false;
            for (List<Integer[]> experiments : userInputs) {
                experiments.sort(new Comparator<Integer[]>() {
                    @Override
                    public int compare(Integer[] o1, Integer[] o2) {
                        return determineCategory(o1) - determineCategory(o2);
                    }

                    private int determineCategory(Integer[] i) {
                        int[][] field = {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1},
                                {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
                                {173, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1},
                                {187, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1},
                                {187, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1},
                                {173, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1},
                                {6, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                                {6, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                                {173, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0},
                                {187, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0},
                                {187, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0},
                                {173, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0},
                                {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {6, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {173, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0},
                                {187, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0},
                                {187, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0},
                                {173, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0},
                                {6, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                                {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                                {173, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0},
                                {187, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0},
                                {187, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0},
                                {173, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0},
                                {6, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        };
                        return field[i[0]][i[1] + 1];
                    }
                });
                Set<NConfig> results = new HashSet<>(), otherList = new HashSet<>(), newResult = new HashSet<>();
                int[] pos = gateToXY(experiments.get(0)[0]);
                clearField.deriveConfiguration(pos[0], pos[1], pos[2], experiments.get(0)[1], new ArrayList<>(), results);
                for (int e = 1; e < experiments.size() && results.size() > 0; e++) {
                    pos = gateToXY(experiments.get(e)[0]);
                    clearField.deriveConfiguration(pos[0], pos[1], pos[2], experiments.get(e)[1], new ArrayList<>(), otherList);
                    for (NConfig r : results) {
                        for (NConfig o : otherList) {
                            NConfig merged = NConfig.merge(r, o);
                            if (merged != null) {
                                newResult.add(merged);
                            }
                        }
                    }
                    results = newResult;
                    newResult = new HashSet<>();
                    otherList.clear();
                }

                for (NConfig r : results) {
                    if (r.getAtomCount() != 5) {
                        if (r.exactlyNUnknown(5 - r.getAtomCount())) {
                            r.convertUnknownToAtom();
                            newResult.add(r);
                        } else if (!(r.getAtomCount() < 5 && r.exactlyNUnknown(0))) {
                            newResult.add(r);
                        }
                    } else {
                        newResult.add(r);
                    }
                    if (newResult.size() > 1)
                        break;
                }
                NConfig[] solutions = new NConfig[newResult.size()];
                newResult.toArray(solutions);
                if (secondExperiment) {
                    output += "\n";
                } else {
                    secondExperiment = true;
                }
                if (solutions.length == 1 && solutions[0].getAtomCount() == 5)
                    output += solutions[0].toCleanString();
                else
                    output += "NO\n";
            }
            System.out.print(output);
            System.out.println(System.currentTimeMillis() - startTime);
        }
    }

    static NConfig merge(NConfig c1, NConfig c2) {
        int[] r = new int[8];
        for (int i = 0; i < 8; i++) {
            r[i] = c1.rows[i] | c2.rows[i];
            if (checkForErrors(r[i])) {
                return null;
            }
        }
        NConfig returnVal = new NConfig(r);
        if (returnVal.atomCount <= 5)
            return new NConfig(r);
        return null;
    }

    private static boolean checkForErrors(int input) {
        for (int i = 0; i < 8; i++) { //go through the row
            if ((input & 0B11) == 0B11) { // if the last two bits are an error
                return true; //there is an error
            }
            input = input >>> 2; //shift the row two bits to the right
        }
        return false; //there is no error
    }

    private static boolean isInBounds(int x, int y) {
        return x >= 0 && y >= 0 && x < 8 && y < 8;
    }

    private static int XYToGate(int x, int y) {
        if (x < 0) {
            return y;
        }
        if (x > 7) {
            return 23 - y;
        }
        if (y < 0) {
            return 31 - x;
        }
        return 8 + x;

    }

    /**
     * @param gate the input gate.
     * @return [0] = x , [1] = y , [2] = dir
     */
    public static int[] gateToXY(int gate) {
        int[] o = new int[3];
        switch (gate / 8) {//Transforming the input into useful information
            case 0:
                o[2] = 0;
                o[0] = 0;
                o[1] = gate;
                break;
            case 1:
                o[2] = 1;
                o[0] = gate % 8;
                o[1] = 7;
                break;
            case 2:
                o[2] = 2;
                o[0] = 7;
                o[1] = 7 - gate % 8;
                break;
            default:
                o[2] = 3;
                o[0] = 7 - gate % 8;
                o[1] = 0;
                break;
        }
        return o;
    }

    void convertUnknownToAtom() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (isTile(i, j, 0B00))
                    setTile(i, j, 0B10);
            }
        }
    }

    boolean exactlyNUnknown(int n) {
        for (int i = 0; i < 8 && n >= 0; i++)
            for (int j = 0; j < 8 && n >= 0; j++)
                if (isTile(i, j, 0B00))
                    n--;
        return n == 0;
    }

    String toCleanString() {
        String output = "";
        for (int i = 0; i < 8; i++) {
            int tmp = rows[i];
            String oneRow = "";
            for (int j = 0; j < 8; j++) {
                switch (tmp & 0B11) {
                    case 0B00://Unknown
                        oneRow = "-" + oneRow;
                        break;
                    case 0B01://Empty
                        oneRow = "-" + oneRow;
                        break;
                    case 0B10://Atom
                        oneRow = "+" + oneRow;
                        break;
                    case 0B11://ERROR
                        oneRow = "E" + oneRow;
                        break;
                }
                tmp = tmp >>> 2;
            }
            output += oneRow + "\n";
        }
        return output;
    }

    public NConfig copy() {
        return new NConfig(Arrays.copyOf(rows, 8), atomCount);
    }

    public void deriveConfiguration(int xStart, int yStart, int dir, int outputGate, List<Integer[]> turningPoints, Set<NConfig> solutions) throws IllegalArgumentException {
        if (atomCount > 5)
            return;
        if (outputGate == performExperiment(xStart, yStart, dir, turningPoints)) {
            NConfig c = copy();
            //Make the clear fields clear (between the first and last turning point)
            c.performActionOnTilesBetweenPoints((x, y) -> c.setTile(x, y, 0B01), turningPoints, 0, turningPoints.size() - 1, true);
            solutions.add(c);
        }
        if (atomCount < 5) {
            //Make the clear fields clear (between the first and n-th turning point,where n is the amount of atoms in the configuration)
            performActionOnTilesBetweenPoints((x, y) -> setTile(x, y, 0B01), turningPoints, 0, atomCount, true);
            List<Integer[]> possiblePositions = new ArrayList<>();
            //Derive the possible positions from the n-th and last turning point
            performActionOnTilesBetweenPoints((x, y) -> possiblePositions.add(new Integer[]{x, y}), turningPoints, atomCount, turningPoints.size() - 1, outputGate == -1);
            //The point from which to start the simulations from
            Integer[] startPoint = turningPoints.get(atomCount);
            for (Integer[] i : possiblePositions) {
                NConfig c = copy();
                c.setTile(i[0], i[1], 0B10);
                c.deriveConfiguration(startPoint[0], startPoint[1], startPoint[2], outputGate, turningPoints.subList(0, atomCount), solutions);
            }
        }
    }

    private void performActionOnTilesBetweenPoints(BiConsumer<Integer, Integer> action, List<Integer[]> points, int fromIndex, int toIndex, boolean includeMiddle) {
        for (int a = fromIndex; a < toIndex; a++) {
            Integer[] m = points.get(a), n = points.get(a + 1);
            if (Objects.equals(m[0], n[0])) {//going vertical
                int increment = m[1] < n[1] ? 1 : -1;
                for (int j = m[1]; j != n[1] + increment; j += increment) {
                    if (isInBounds(m[0] - 1, j) && isTile(m[0] - 1, j, 0B00)) {
                        action.accept(m[0] - 1, j);
                    }
                    if (isInBounds(m[0] + 1, j) && isTile(m[0] + 1, j, 0B00)) {
                        action.accept(m[0] + 1, j);
                    }
                    if (includeMiddle && isInBounds(m[0], j) && isTile(m[0], j, 0B00)) {
                        action.accept(m[0], j);
                    }
                }
                if (includeMiddle && isInBounds(m[0], n[1] + increment) && isTile(m[0], n[1] + increment, 0B00)) {
                    action.accept(m[0], n[1] + increment);
                }
            }
            if (Objects.equals(m[1], n[1])) {//going horizontal
                int increment = m[0] < n[0] ? 1 : -1;
                for (int i = m[0]; i != n[0] + increment; i += increment) {
                    if (isInBounds(i, m[1] - 1) && isTile(i, m[1] - 1, 0B00)) {
                        action.accept(i, m[1] - 1);
                    }
                    if (isInBounds(i, m[1] + 1) && isTile(i, m[1] + 1, 0B00)) {
                        action.accept(i, m[1] + 1);
                    }
                    if (includeMiddle && isInBounds(i, m[1]) && isTile(i, m[1], 0B00)) {
                        action.accept(i, m[1]);
                    }
                }
                if (includeMiddle && isInBounds(n[0] + increment, m[1]) && isTile(n[0] + increment, m[1], 0B00)) {
                    action.accept(n[0] + increment, m[1]);
                }
            }
        }
    }

    /**
     * Performs an experiment on the configuration following the rules given in the task description. In this case
     * the laser does not start from a gate, but instead from any given tile.
     *
     * @param x             The starting x-coordinate. 0<=x<=7
     * @param y             The starting y-coordinate. 0<=y<=7
     * @param dir           The starting direction. 0=Right,1=Up,2=Left,3=Down
     * @param turningPoints The list of turning points to add to.
     * @return The gate through which the laser exits, or -1 if it is absorbed.
     */
    public int performExperiment(int x, int y, int dir, List<Integer[]> turningPoints) {
        //Adding the starting point to the list
        turningPoints.add(new Integer[]{x, y, dir});
        while (isInBounds(x, y)) {
            //Calculate the next position based on the current coordinates and direction
            int newX = x + Math.abs(dir - 2) - 1, newY = y + Math.abs(dir - 1) - 1;
            //Checking if the next position is an atom
            if (isInBounds(newX, newY) && isTile(newX, newY, 0B10)) {
                turningPoints.add(new Integer[]{x, y, dir});
                turningPoints.add(new Integer[]{x, y, dir});
                return -1;
            }
            //checking for direction changing atoms. reversing the movement if there are any
            if (dir % 2 == 0) {//going horizontal
                if (isInBounds(newX, newY - 1) && isTile(newX, newY - 1, 0B10)) {//Atom above
                    newX = x;
                    dir = 3;//Down
                    turningPoints.add(new Integer[]{x, y, dir});
                } else if (isInBounds(newX, newY + 1) && isTile(newX, newY + 1, 0B10)) {//Atom below
                    newX = x;
                    dir = 1;//Up
                    turningPoints.add(new Integer[]{x, y, dir});
                }
            } else {//going vertical
                if (isInBounds(newX - 1, newY) && isTile(newX - 1, newY, 0B10)) {//Atom on the left
                    newY = y;
                    dir = 0;
                    turningPoints.add(new Integer[]{x, y, dir});
                } else if (isInBounds(newX + 1, newY) && isTile(newX + 1, newY, 0B10)) {//Atom on the right
                    newY = y;
                    dir = 2;
                    turningPoints.add(new Integer[]{x, y, dir});
                }
            }
            //setting the new position
            x = newX;
            y = newY;
        }
        //Adding the end point to the list
        turningPoints.add(new Integer[]{x, y, dir});
        //Reverting the coordinates to a gate
        return XYToGate(x, y);
    }

    /**
     * Checks if a tile at a position is of a specific type.
     *
     * @param x    The x-coordinate of the tile.
     * @param y    The y-coordinate of the tile.
     * @param type The type of the Utils.Tile.
     * @return Whether it is of the wanted type or not.
     */
    public boolean isTile(int x, int y, int type) {
        //             row of the tile    x-coordinate  bitmask
        return type == ((rows[y] >>> (2 * (7 - x))) & 0B11);
        //             length of a tile inside the store
    }

    public void setTile(int x, int y, int type) {
        if (type == 0B10 && !isTile(x, y, type)) { //updating the atom count if necessary
            atomCount++;
        }
        if (isInBounds(x, y)) {//checking for valid input
            //     current row|new Utils.Tile
            rows[y] = rows[y] | (type << (2 * (7 - x)));
            //                          Shifting it to the right by the correct amount
        }
    }

    public void forceSetTile(int x, int y, int type) {
        if (type == 0B10 && !isTile(x, y, type)) { //updating the atom count if necessary
            atomCount++;
        }

        if (isInBounds(x, y)) {//checking for valid input
            if (type != 0B10 && isTile(x, y, 0B10))
                atomCount--;
            //     current row|new Utils.Tile
            rows[y] -= rows[y] & (0b11 << (2 * (7 - x)));
            //                          Shifting it to the right by the correct amount
            rows[y] = rows[y] | (type << (2 * (7 - x)));
        }
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof NConfig && Arrays.equals(rows, ((NConfig) o).rows);
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < 8; i++) {
            int tmp = rows[i];
            String oneRow = "";
            for (int j = 0; j < 8; j++) {
                switch (tmp & 0B11) {
                    case 0B00://Unknown
                        oneRow = "?" + oneRow;
                        break;
                    case 0B01://Empty
                        oneRow = "-" + oneRow;
                        break;
                    case 0B10://Atom
                        oneRow = "+" + oneRow;
                        break;
                    case 0B11://ERROR
                        oneRow = "E" + oneRow;
                        break;
                }
                tmp = tmp >>> 2;
            }
            output += oneRow + "\n";
        }
        return output;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(rows);
    }

    int getAtomCount() {
        return atomCount;
    }
}
