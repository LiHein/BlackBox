package Configs;

import java.io.IOException;
import java.util.*;
import java.util.function.BiConsumer;

/**
 * This is going to be the clean version of the Blackbox-solution
 * Created by Linus Hein on 05.06.2016.
 */
@SuppressWarnings("Duplicates")
public class Configuration {
    private int[] rows; //Two bits represent a tile: 00 = unknown, 01 = empty, 10 = atom , 11 = error
    private int atomCount;

    /**
     * Creates an empty configuration.
     */
    Configuration() {
        atomCount = 0;
        rows = new int[8];
        rows[0] = rows[7] = 0b0101010101010101;
        for (int i = 1; i < 7; i++) {
            rows[i] = 0B01_00_00_00_00_00_00_01;
        }
    }


    /**
     * Creates an configuration based on the given rows. Can be inefficient, because it calculates the amount of atoms
     * 'by hand'.
     *
     * @param rows An array of length 8 representing the rows of the configuration.
     */
    private Configuration(int[] rows) {
        this.rows = rows;
        atomCount = 0;
        for (int i = 1; i < 7; i++) {
            int tmp = rows[i]>>>2;
            //while (tmp > 0) {
            for(int j=1;j<7;j++) {
                if ((tmp & 0B11) == 0B10) {
                    atomCount++;
                }
                tmp = tmp >>> 2;
            }
        }
    }

    /**
     * Creates an configuration based on the given rows. This is more efficient, because it does not have to count the
     * amount of atoms in the configuration.
     *
     * @param rows      An array of length 8 representing the rows of the configuration.
     * @param atomCount The amount of atoms inside the given configuration.
     */
    private Configuration(int[] rows, int atomCount) {
        this.rows = rows;
        this.atomCount = atomCount;
    }

    static List<List<Integer[]>> readInput() {
        List<List<Integer[]>> userInputs = new ArrayList<>();
        int amountConf = readNumber("\\n");
        for (int i = 0; i < amountConf; i++) {
            readNumber("\\n");
            int amountOfExperiments = readNumber("\\n");
            List<Integer[]> setOfExperiments = new ArrayList<>();
            for (int j = 0; j < amountOfExperiments; j++) {
                setOfExperiments.add(new Integer[]{readNumber(" "), readNumber("\\n")});
            }
            userInputs.add(setOfExperiments);
        }
        return userInputs;
    }

    private static int readNumber(String terminatedBy) {
        String input = "";
        try {
            while (!input.matches("-??\\d*" + terminatedBy)) {
                input += (char) System.in.read();
            }
        } catch (IOException e) {
            return -1;
        }
        if (input.length() < 2)
            return 0;
        if (input.matches("-??\\d*\\s"))
            return Integer.parseInt(input.substring(0, input.length() - 1));
        return Integer.parseInt(input);
    }

    public static void main(String[] args) {
        System.out.println(solveRebase(readInput()));
    }

    private static List<List<Integer[]>> copyUserInput(List<List<Integer[]>> userInput) {
        List<List<Integer[]>> output = new ArrayList<>();
        for (List<Integer[]> a : userInput) {
            List<Integer[]> element = new ArrayList<>();
            for (Integer[] b : a) {
                Integer[] copy = new Integer[2];
                copy[0] = new Integer(b[0].intValue());
                copy[1] = new Integer(b[1].intValue());
                element.add(copy);
            }
            output.add(element);
        }
        return output;
    }

    public static String solveMerge(List<List<Integer[]>> userInputs) {
        String output = "";
        for (List<Integer[]> input : userInputs) {
            //Sorting the input
            input.sort(new Comparator<Integer[]>() {
                @Override
                public int compare(Integer[] o1, Integer[] o2) {
                    return determineCategory(o1) - determineCategory(o2);
                }

                private int determineCategory(Integer[] i) {
                    int field[][] = new int[][]{
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
                            {173, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1},
                            {187, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1},
                            {187, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1},
                            {173, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                            {6, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                            {173, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0},
                            {187, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0},
                            {187, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0},
                            {173, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {6, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {173, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0},
                            {187, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0},
                            {187, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0},
                            {173, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0},
                            {6, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                            {173, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0},
                            {187, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0},
                            {187, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0},
                            {173, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0},
                            {6, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                    };
                    return field[i[0]][i[1] + 1];
                }
            });
            //getting all solutions for the current input
            Configuration clear = new Configuration();
            Set<Configuration> solutions = new HashSet<>(),
                    tmp = new HashSet<>(),
                    otherSolutions = new HashSet<>();
            solutions.add(new Configuration());
            for (Integer[] experiment : input) {
                otherSolutions.addAll(clear.deriveConfiguration(experiment));
                for (Configuration a : solutions) {
                    for (Configuration b : otherSolutions) {
                        Configuration merged = Configuration.merge(a, b);
                        if (merged != null) {
                            tmp.add(merged);
                        }
                    }
                }
                solutions.clear();
                solutions.addAll(tmp);
                otherSolutions.clear();
                tmp.clear();
            }
            output += makeExperimentOutput(solutions);
        }
        return output;
    }

    static Configuration merge(Configuration c1, Configuration c2) {
        int[] rows = new int[]{
                0b0101010101010101,//first row is always empty
                c1.rows[1] | c2.rows[1],
                c1.rows[2] | c2.rows[2],
                c1.rows[3] | c2.rows[3],
                c1.rows[4] | c2.rows[4],
                c1.rows[5] | c2.rows[5],
                c1.rows[6] | c2.rows[6],
                0b0101010101010101};//last row is always empty
        for (int i = 1; i < 7; i++) {
            if (checkForErrors(rows[i])) {
                return null;
            }
        }
        Configuration returnVal = new Configuration(rows);
        if (returnVal.atomCount <= 5)
            return returnVal;
        return null;
    }

    private static boolean checkForErrors(int input) {
        input = input >>> 2; //skip the rim
        for (int i = 0; i < 6; i++) { //go through the row
            if ((input & 0B11) == 0B11) { // if the last two bits are an error
                return true; //there is an error
            }
            input = input >>> 2; //shift the row two bits to the right
        }
        return false; //there is no error
    }

    public static String solveRebase(List<List<Integer[]>> userInputs) {
        String output = "";
        for (List<Integer[]> input : userInputs) {
            //Sorting the input
            input.sort(new Comparator<Integer[]>() {
                @Override
                public int compare(Integer[] o1, Integer[] o2) {
                    return determineCategory(o1) - determineCategory(o2);
                }

                private int determineCategory(Integer[] i) {
                    int field[][] = new int[][]{
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
                            {173, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1},
                            {187, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1},
                            {187, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1},
                            {173, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                            {6, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                            {173, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0},
                            {187, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0},
                            {187, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0},
                            {173, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {6, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {173, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0, 1, 1, 8, 8, 15, 20, 0, 0},
                            {187, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0, 1, 1, 4, 10, 17, 15, 0, 0},
                            {187, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0, 1, 1, 10, 11, 10, 8, 0, 0},
                            {173, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0, 1, 1, 19, 10, 4, 8, 0, 0},
                            {6, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                            {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                            {173, 1, 1, 8, 8, 15, 20, 0, 0, 0, 0, 16, 12, 8, 10, 0, 0, 0, 0, 8, 4, 10, 19, 1, 1, 0, 0, 87, 20, 13, 6, 0, 0},
                            {187, 1, 1, 4, 10, 17, 15, 0, 0, 0, 0, 12, 8, 10, 8, 0, 0, 0, 0, 8, 10, 11, 10, 1, 1, 0, 0, 20, 96, 20, 13, 0, 0},
                            {187, 1, 1, 10, 11, 10, 8, 0, 0, 0, 0, 8, 10, 8, 12, 0, 0, 0, 0, 15, 17, 10, 4, 1, 1, 0, 0, 13, 20, 96, 20, 0, 0},
                            {173, 1, 1, 19, 10, 4, 8, 0, 0, 0, 0, 10, 8, 12, 16, 0, 0, 0, 0, 20, 15, 8, 8, 1, 1, 0, 0, 6, 13, 20, 87, 0, 0},
                            {6, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                    };
                    return field[i[0]][i[1] + 1];
                }
            });
            //getting all solutions for the current input
            Set<Configuration> solutions = new HashSet<>(),
                    tmp = new HashSet<>();
            solutions.add(new Configuration());
            for (Integer[] experiment : input) {
                for (Configuration c : solutions)
                    tmp.addAll(c.deriveConfiguration(experiment));
                solutions.clear();
                solutions.addAll(tmp);
                tmp.clear();
            }
            output += makeExperimentOutput(solutions) + (input == userInputs.get(userInputs.size() - 1) ? "" : "\n");
        }
        return output;
    }

    private static String makeExperimentOutput(Set<Configuration> solutions) {
        Set<Configuration> tmp = new HashSet<>();
        //getting rid of invalid solutions
        for (Configuration c : solutions) {
            if (c.getAtomCount() == 5) {
                tmp.add(c);
            } else {
                if (c.exactlyNUnknown(5 - c.getAtomCount())) {
                    c.convertUnknownToAtom();
                    tmp.add(c);
                } else if (!(c.getAtomCount() < 5 && c.exactlyNUnknown(0))) {
                    tmp.add(c);
                }
            }
            if (tmp.size() > 1) break;
        }
        Configuration[] results = new Configuration[tmp.size()];
        tmp.toArray(results);
        String output = "";
        if (results.length == 1 && results[0].getAtomCount() == 5)
            output += results[0].toCleanString();
        else
            output += "NO\n";
        return output;
    }

    private static boolean isInBounds(int x, int y) {
        return x >= 0 && y >= 0 && x < 8 && y < 8;
    }

    private static int XYToGate(int x, int y) {
        if (x < 0) {
            return y;
        }
        if (x > 7) {
            return 23 - y;
        }
        if (y < 0) {
            return 31 - x;
        }
        return 8 + x;

    }

    static Integer[] gateToXY(int gate) {
        Integer[] o = new Integer[3];
        switch (gate / 8) {//Transforming the input into useful information
            case 0:
                o[2] = 0;
                o[0] = 0;
                o[1] = gate;
                break;
            case 1:
                o[2] = 1;
                o[0] = gate % 8;
                o[1] = 7;
                break;
            case 2:
                o[2] = 2;
                o[0] = 7;
                o[1] = 7 - gate % 8;
                break;
            default:
                o[2] = 3;
                o[0] = 7 - gate % 8;
                o[1] = 0;
                break;
        }
        return o;
    }

    Set<Configuration> deriveConfiguration(Integer[] experiment) {
        Integer[] dir = gateToXY(experiment[0]);
        return deriveConfiguration(0, dir, experiment[1], new ArrayList<>());
    }

    Set<Configuration> deriveConfiguration(int setAtoms, Integer[] dir, int outputGate, List<Integer[]> turningPoints) {
        Set<Configuration> solutions = new HashSet<>();
        if (outputGate == performExperiment(dir, turningPoints)) {
            Configuration copy = copy();
            //mark tiles next to path as clear
            copy.performAction((x, y) -> copy.setTile(x, y, 0B01), turningPoints, 0, turningPoints.size() - 1, true);
            solutions.add(copy);
        }
        if (atomCount < 5) {
            //mark the path up to now as clear
            performAction((x, y) -> setTile(x, y, 0B01), turningPoints, 0, setAtoms, true);
            //get all the possible positions for the next atom
            List<Integer[]> possiblePositions = new ArrayList<>();
            performAction((x, y) -> possiblePositions.add(new Integer[]{x, y}), turningPoints, setAtoms, turningPoints.size() - 1, outputGate == -1);
            //The starting point for future simulations
            Integer[] startPoint = turningPoints.get(setAtoms);
            for (Integer[] position : possiblePositions) {
                Configuration copy = copy();
                copy.setTile(position[0], position[1], 0B10);
                solutions.addAll(copy.deriveConfiguration(setAtoms + 1, startPoint, outputGate, turningPoints.subList(0, setAtoms)));
            }
        }
        return solutions;
    }

    private void performAction(BiConsumer<Integer, Integer> action, List<Integer[]> points, int fromIndex, int toIndex, boolean includeMiddle) {
        for (int a = fromIndex; a < toIndex; a++) {
            Integer[] m = points.get(a), n = points.get(a + 1);
            if (Objects.equals(m[0], n[0])) {//going vertical
                int increment = m[1] < n[1] ? 1 : -1;
                for (int j = m[1]; j != n[1] + increment; j += increment) {
                    if (isInBounds(m[0] - 1, j) && isTile(m[0] - 1, j, 0B00)) {
                        action.accept(m[0] - 1, j);
                    }
                    if (isInBounds(m[0] + 1, j) && isTile(m[0] + 1, j, 0B00)) {
                        action.accept(m[0] + 1, j);
                    }
                    if (includeMiddle && isInBounds(m[0], j) && isTile(m[0], j, 0B00)) {
                        action.accept(m[0], j);
                    }
                }
                if (includeMiddle && isInBounds(m[0], n[1] + increment) && isTile(m[0], n[1] + increment, 0B00)) {
                    action.accept(m[0], n[1] + increment);
                }
            }
            if (Objects.equals(m[1], n[1])) {//going horizontal
                int increment = m[0] < n[0] ? 1 : -1;
                for (int i = m[0]; i != n[0] + increment; i += increment) {
                    if (isInBounds(i, m[1] - 1) && isTile(i, m[1] - 1, 0B00)) {
                        action.accept(i, m[1] - 1);
                    }
                    if (isInBounds(i, m[1] + 1) && isTile(i, m[1] + 1, 0B00)) {
                        action.accept(i, m[1] + 1);
                    }
                    if (includeMiddle && isInBounds(i, m[1]) && isTile(i, m[1], 0B00)) {
                        action.accept(i, m[1]);
                    }
                }
                if (includeMiddle && isInBounds(n[0] + increment, m[1]) && isTile(n[0] + increment, m[1], 0B00)) {
                    action.accept(n[0] + increment, m[1]);
                }
            }
        }
    }

    /**
     * Performs an experiment on the configuration following the rules given in the task description. In this case
     * the laser does not start from a gate, but instead from any given tile.
     * <p>
     * x             The starting x-coordinate. 0<=x<=7
     * y             The starting y-coordinate. 0<=y<=7
     * dir           The starting direction. 0=Right,1=Up,2=Left,3=Down
     *
     * @param start         The starting point [0]=x,[1]=y,[2]=direction
     * @param turningPoints The list of turning points to add to.
     * @return The gate through which the laser exits, or -1 if it is absorbed.
     */
    int performExperiment(Integer[] start, List<Integer[]> turningPoints) {
        int x = start[0], y = start[1], dir = start[2];
        //Adding the starting point to the list
        turningPoints.add(new Integer[]{x, y, dir});
        while (isInBounds(x, y)) {
            //Calculate the next position based on the current coordinates and direction
            int newX = x + Math.abs(dir - 2) - 1, newY = y + Math.abs(dir - 1) - 1;
            //Checking if the next position is an atom
            if (isInBounds(newX, newY) && isTile(newX, newY, 0B10)) {
                turningPoints.add(new Integer[]{x, y, dir});
                turningPoints.add(new Integer[]{x, y, dir});
                return -1;
            }
            //checking for direction changing atoms. reversing the movement if there are any
            if (dir % 2 == 0) {//going horizontal
                if (isInBounds(newX, newY - 1) && isTile(newX, newY - 1, 0B10)) {//Atom above
                    newX = x;
                    dir = 3;//Down
                    turningPoints.add(new Integer[]{x, y, dir});
                } else if (isInBounds(newX, newY + 1) && isTile(newX, newY + 1, 0B10)) {//Atom below
                    newX = x;
                    dir = 1;//Up
                    turningPoints.add(new Integer[]{x, y, dir});
                }
            } else {//going vertical
                if (isInBounds(newX - 1, newY) && isTile(newX - 1, newY, 0B10)) {//Atom on the left
                    newY = y;
                    dir = 0;
                    turningPoints.add(new Integer[]{x, y, dir});
                } else if (isInBounds(newX + 1, newY) && isTile(newX + 1, newY, 0B10)) {//Atom on the right
                    newY = y;
                    dir = 2;
                    turningPoints.add(new Integer[]{x, y, dir});
                }
            }
            //setting the new position
            x = newX;
            y = newY;
        }
        //Adding the end point to the list
        turningPoints.add(new Integer[]{x, y, dir});
        //Reverting the coordinates to a gate
        return XYToGate(x, y);
    }

    String toCleanString() {
        String output = "";
        for (int i = 0; i < 8; i++) {
            int tmp = rows[i];
            String oneRow = "";
            for (int j = 0; j < 8; j++) {
                switch (tmp & 0B11) {
                    case 0B00://Unknown
                        oneRow = "-" + oneRow;
                        break;
                    case 0B01://Empty
                        oneRow = "-" + oneRow;
                        break;
                    case 0B10://Atom
                        oneRow = "+" + oneRow;
                        break;
                    case 0B11://ERROR
                        oneRow = "E" + oneRow;
                        break;
                }
                tmp = tmp >>> 2;
            }
            output += oneRow + "\n";
        }
        return output;
    }

    int getAtomCount() {
        return atomCount;
    }

    /**
     * Checks if a tile at a position is of a specific type.
     *
     * @param x    The x-coordinate of the tile.
     * @param y    The y-coordinate of the tile.
     * @param type The type of the Utils.Tile.
     * @return Whether it is of the wanted type or not.
     */
    boolean isTile(int x, int y, int type) {
        //             row of the tile    x-coordinate  bitmask
        return type == ((rows[y] >>> (2 * (7 - x))) & 0B11);
        //             length of a tile inside the store
    }

    /**
     * Sets the tile at x|y, returning the previous value of this tile
     *
     * @param x    x-coordinate of the tile
     * @param y    y-coordinate of the tile
     * @param type new type of the tile
     */
    void setTile(int x, int y, int type) {
        if (type == 0B10 && !isTile(x, y, type)) { //updating the atom count if necessary
            atomCount++;
        }
        if (isInBounds(x, y)) {//checking for valid input
            //     current row|new Utils.Tile
            rows[y] = rows[y] | (type << (2 * (7 - x)));
            //                          Shifting it to the right by the correct amount
        }
    }

    Configuration copy() {
        return new Configuration(Arrays.copyOf(rows, 8), atomCount);
    }

    void convertUnknownToAtom() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (isTile(i, j, 0B00))
                    setTile(i, j, 0B10);
            }
        }
    }

    boolean exactlyNUnknown(int n) {
        for (int i = 0; i < 8 && n >= 0; i++)
            for (int j = 0; j < 8 && n >= 0; j++)
                if (isTile(i, j, 0B00))
                    n--;
        return n == 0;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Configuration && Arrays.equals(rows, ((Configuration) o).rows);
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < 8; i++) {
            int tmp = rows[i];
            String oneRow = "";
            for (int j = 0; j < 8; j++) {
                switch (tmp & 0B11) {
                    case 0B00://Unknown
                        oneRow = "?" + oneRow;
                        break;
                    case 0B01://Empty
                        oneRow = "-" + oneRow;
                        break;
                    case 0B10://Atom
                        oneRow = "+" + oneRow;
                        break;
                    case 0B11://ERROR
                        oneRow = "E" + oneRow;
                        break;
                }
                tmp = tmp >>> 2;
            }
            output += oneRow + "\n";
        }
        return output;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(rows);
    }
}
