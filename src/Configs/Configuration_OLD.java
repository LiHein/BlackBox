package Configs;

import Utils.Direction;
import Utils.Tile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static Utils.Direction.*;
import static Utils.Tile.*;

/**
 * A configuration is a two-dimensional Array of Tiles. You can perform experiments on it and derive new Configurations
 * from a given Configs.Configuration and an experiment.
 * Created by linus on 12.03.2016.
 */
public class Configuration_OLD {
    private static Direction possibleDirections[] = Direction.values();
    private Tile[][] field;
    private int amountOfAtoms;

    public Configuration_OLD(Tile[][] field) {
        this.field = field;
        updateAtomCount();
    }

    /**
     * Returns an empty configuration. This means the outer rim is completely empty and the inside is unknown.
     *
     * @return empty configuration.
     */
    public static Tile[][] empty() {
        Tile[][] output = new Tile[8][8];
        //clearing the entire field
        for (int i = 1; i < 7; i++) {
            for (int j = 1; j < 7; j++) {
                output[i][j] = Unknown;
            }
        }
        //emptying the outer rim
        for (int i = 0; i < 8; i++) {
            output[i][0] = output[0][i] = output[i][7] = output[7][i] = Empty;
        }
        return output;
    }

    public static Configuration_OLD merge(Configuration_OLD c1, Configuration_OLD c2) {
        Tile[][] field = empty();
        for (int i = 1; i < 7; i++) {
            for (int j = 1; j < 7; j++) {
                if (c1.getTile(i, j) == Unknown) {
                    field[i][j] = c2.getTile(i, j);
                } else if (c2.getTile(i, j) == Unknown) {
                    field[i][j] = c1.getTile(i, j);
                } else if (c1.getTile(i, j) == c2.getTile(i, j)) {
                    field[i][j] = c1.getTile(i, j);
                } else
                    return null;
            }
        }
        Configuration_OLD returnVal = new Configuration_OLD(field);
        if (returnVal.getAmountOfAtoms() <= 5)
            return new Configuration_OLD(field);
        return null;
    }

    /**
     * Checks whether the given coordinates are sensible.
     *
     * @param x x-Coordinate
     * @param y y-Coordinate
     * @return returns true if 0<=x<=7 && 0<=y<=7
     */
    private static boolean isInBounds(int x, int y) {
        return x >= 0 && x < 8 && y >= 0 && y < 8;
    }

    /**
     * Makes a list of all configurations based on the current one that will fulfil the given experiment.
     *
     * @param inGate  Input-Gate of the experiment.
     * @param outGate Output-Gate of the experiment.
     * @param list    The list to add the configurations into.
     */
    public void deriveConfiguration(int inGate, int outGate, List<Configuration_OLD> list) {
        if (amountOfAtoms > 5)
            return;
        List<Integer[]> possiblePositions = new ArrayList<>();
        List<Integer[]> turningPoints = getTurningPoints(inGate);
        if (outGate == performExperiment(inGate)) {
            Configuration_OLD c = copy();
            c.clearPath((ArrayList<Integer[]>) turningPoints, 0, turningPoints.size() - 1, Empty, true);
            list.add(c);
        }
        //Changes the state of empty tiles to Utils.Tile.Empty
        clearPath((ArrayList<Integer[]>) turningPoints, 0, turningPoints.size() - 2, Empty, true);
        Integer[] m = turningPoints.get(turningPoints.size() - 2), n = turningPoints.get(turningPoints.size() - 1);
        if (Objects.equals(m[0], n[0])) {//going vertical
            int increment = m[1] < n[1] ? 1 : -1;
            for (int j = m[1]; j != n[1] + increment; j += increment) {
                if (isInBounds(m[0] - 1, j) && field[m[0] - 1][j] == Unknown) {
                    possiblePositions.add(new Integer[]{m[0] - 1, j});
                }
                if (isInBounds(m[0] + 1, j) && field[m[0] + 1][j] == Unknown) {
                    possiblePositions.add(new Integer[]{m[0] + 1, j});
                }
                if (outGate == -1 && isInBounds(m[0], j) && field[m[0]][j] == Unknown) {
                    possiblePositions.add(new Integer[]{m[0], j});
                }
            }
        } else {//going horizontal
            int increment = m[0] < n[0] ? 1 : -1;
            for (int i = m[0]; i != n[0]; i += increment) {
                if (isInBounds(i, m[1] - 1) && field[i][m[1] - 1] == Unknown) {
                    possiblePositions.add(new Integer[]{i, m[1] - 1});
                }
                if (isInBounds(i, m[1] + 1) && field[i][m[1] + 1] == Unknown) {
                    possiblePositions.add(new Integer[]{i, m[1] + 1});
                }
                if (outGate == -1 && isInBounds(i, m[1]) && field[i][m[1]] == Unknown) {
                    possiblePositions.add(new Integer[]{i, m[1]});
                }
            }
        }
        //Tests all the possible positions
        for (Integer[] i : possiblePositions) {
            Configuration_OLD c = copy();
            c.setTile(i[0], i[1], Atom);
            c.deriveConfiguration(inGate, outGate, list);
        }
    }

    /**
     * @param points        The list of connected points in proper order.
     * @param fromIndex     Index of the list to start from (inclusive)
     * @param toIndex       exclusive
     * @param clearingTile  The tile to clear with
     * @param includeMiddle Whether or not the middle tile should be cleared, too
     */
    private void clearPath(ArrayList<Integer[]> points, int fromIndex, int toIndex, Tile clearingTile, boolean includeMiddle) {
        for (int a = fromIndex; a < toIndex; a++) {
            Integer[] m = points.get(a), n = points.get(a + 1);
            if (Objects.equals(m[0], n[0])) {//going vertical
                int increment = m[1] < n[1] ? 1 : -1;
                for (int j = m[1]; j != n[1] + increment; j += increment) {
                    if (isInBounds(m[0] - 1, j) && field[m[0] - 1][j] == Unknown) {
                        field[m[0] - 1][j] = clearingTile;
                    }
                    if (isInBounds(m[0] + 1, j) && field[m[0] + 1][j] == Unknown) {
                        field[m[0] + 1][j] = clearingTile;
                    }
                    if (isInBounds(m[0], j) && field[m[0]][j] == Unknown && includeMiddle)
                        field[m[0]][j] = clearingTile;
                }
            } else {//going horizontal
                int increment = m[0] < n[0] ? 1 : -1;
                for (int i = m[0]; i != n[0] + increment; i += increment) {
                    if (isInBounds(i, m[1] - 1) && field[i][m[1] - 1] == Unknown) {
                        field[i][m[1] - 1] = clearingTile;
                    }
                    if (isInBounds(i, m[1] + 1) && field[i][m[1] + 1] == Unknown) {
                        field[i][m[1] + 1] = clearingTile;
                    }
                    if (isInBounds(i, m[1]) && field[i][m[1]] == Unknown && includeMiddle)
                        field[i][m[1]] = clearingTile;
                }
            }
        }
    }

    /**
     * Performs an experiment according to the rules of the task description. Unknown fields will be handled as if
     * nothing were in them.
     *
     * @param inputGate the input gate.
     * @return The output gate or -1 if absorbed.
     */
    private int performExperiment(int inputGate) throws IllegalArgumentException {
        if (inputGate < 0 || inputGate > 31)
            throw new IllegalArgumentException("The parameter inputGate should be >=0 and <=31, but is " + inputGate);
        int[] val = NConfig.gateToXY(inputGate);
        int x = val[0], y = val[1];
        Direction dir = possibleDirections[val[2]];
        //Simulating the situation
        while (isInBounds(x, y)) {
            int newX = x, newY = y;
            switch (dir) {
                case Up:
                    newY -= 1;
                    break;
                case Down:
                    newY += 1;
                    break;
                case Right:
                    newX += 1;
                    break;
                case Left:
                    newX -= 1;
                    break;
            }
            if (isInBounds(newX, newY) && field[newX][newY] == Atom) //just crashed into an atom
                return -1;
            if (dir == Up || dir == Down) { //going vertical
                if (isInBounds(newX - 1, newY) && field[newX - 1][newY] == Atom) {
                    dir = Right;
                    newY = y;
                } else if (isInBounds(newX + 1, newY) && field[newX + 1][newY] == Atom) {
                    dir = Left;
                    newY = y;
                }
            } else { // going horizontal
                if (isInBounds(newX, newY - 1) && field[newX][newY - 1] == Atom) {
                    dir = Down;
                    newX = x;
                } else if (isInBounds(newX, newY + 1) && field[newX][newY + 1] == Atom) {
                    dir = Up;
                    newX = x;
                }
            }
            x = newX;
            y = newY;
        }
        //Determining the output gate
        return XYToGate(x, y);
    }

    private List<Integer[]> getTurningPoints(int inputGate) throws IllegalArgumentException {
        if (inputGate < 0 || inputGate > 31)
            throw new IllegalArgumentException("The parameter inputGate should be >=0 and <=31, but is " + inputGate);
        int[] val = NConfig.gateToXY(inputGate);
        int x = val[0], y = val[1];
        Direction dir = possibleDirections[val[2]];
        List<Integer[]> output = new ArrayList<>();
        output.add(new Integer[]{x, y});
        //Simulating the situation
        while (isInBounds(x, y)) {
            int newX = x, newY = y;
            switch (dir) {
                case Up:
                    newY -= 1;
                    break;
                case Down:
                    newY += 1;
                    break;
                case Right:
                    newX += 1;
                    break;
                case Left:
                    newX -= 1;
                    break;
            }
            if (isInBounds(newX, newY) && field[newX][newY] == Atom) { //just crashed into an atom
                output.add(new Integer[]{x, y});
                break;
            }
            if (dir == Up || dir == Down) { //going vertical
                if (isInBounds(newX - 1, newY) && field[newX - 1][newY] == Atom) {
                    output.add(new Integer[]{x, y});
                    dir = Right;
                    newY = y;
                } else if (isInBounds(newX + 1, newY) && field[newX + 1][newY] == Atom) {
                    output.add(new Integer[]{x, y});
                    dir = Left;
                    newY = y;
                }
            } else { // going horizontal
                if (isInBounds(newX, newY - 1) && field[newX][newY - 1] == Atom) {
                    output.add(new Integer[]{x, y});
                    dir = Down;
                    newX = x;
                } else if (isInBounds(newX, newY + 1) && field[newX][newY + 1] == Atom) {
                    output.add(new Integer[]{x, y});
                    dir = Up;
                    newX = x;
                }
            }
            x = newX;
            y = newY;
        }
        output.add(new Integer[]{x, y});
        return output;
    }

    private int XYToGate(int x, int y) {
        if (x < 0) {
            return y;
        }
        if (x > 7) {
            return 23 - y;
        }
        if (y < 0) {
            return 31 - x;
        }
        return 8 + x;

    }

    private void setTile(int x, int y, Tile type) {
        field[x][y] = type;
        if (type == Atom)
            updateAtomCount();
    }

    private Configuration_OLD copy() {
        Tile[][] f = new Tile[8][8];
        for (int i = 0; i < 8; i++)
            System.arraycopy(field[i], 0, f[i], 0, 8);
        return new Configuration_OLD(f);
    }

    private Tile getTile(int x, int y) {
        return field[x][y];
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < 8; i++) {//Column
            for (int j = 0; j < 8; j++) {//Row
                switch (field[j][i]) {
                    case Unknown:
                        output += "? ";
                        break;
                    case Atom:
                        output += "+ ";
                        break;
                    case Empty:
                        output += "- ";
                        break;
                }
            }
            output += "\n";
        }
        return output;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Configuration_OLD) {
            Configuration_OLD c = (Configuration_OLD) o;
            return Arrays.deepEquals(field, c.field);
        }
        return false;
    }

    private void updateAtomCount() {
        amountOfAtoms = 0;
        for (int i = 1; i < 7; i++) {
            for (int j = 1; j < 7; j++) {
                if (field[i][j] == Atom)
                    amountOfAtoms++;
            }
        }
    }

    private int getAmountOfAtoms() {
        return amountOfAtoms;
    }
}
