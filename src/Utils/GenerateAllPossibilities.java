package Utils;

import Configs.NConfig;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by linus on 14.05.2016.
 */
public class GenerateAllPossibilities {
    public static NConfig[] possibilities = new NConfig[376992];
    public static int gates[][] = new int[376992][32], inOut[][] = new int[32][33];

    public static void main(String[] args) {
        generateAllConfigurations();
    }

    public static void generateAllPartialConfigs() {
        NConfig n = new NConfig();
        Set<NConfig> solutions = new HashSet<>();
        System.out.println("{");
        for (int inGate = 0; inGate < 32; inGate++) {
            int values[] = NConfig.gateToXY(inGate);
            System.out.print("{");
            for (int outGate = -1; outGate < 32; outGate++) {
                solutions.clear();
                n.deriveConfiguration(values[0], values[1], values[2], outGate, new ArrayList<>(), solutions);
                System.out.print(solutions.size() + (outGate != 31 ? "," : ""));
            }
            System.out.println("}" + (inGate != 31 ? "," : ""));
        }
        System.out.println("}");
    }

    public static void generateAllConfigurations() {
        NConfig n = new NConfig();
        for (int i = 1; i < 7; i++) {
            for (int j = 1; j < 7; j++) {
                n.setTile(i, j, 0b01);
            }
        }
        generate(n, -1, 0, possibilities, 0);
        for (int i = 0; i < 376992; i++) {
            for (int inGate = 0; inGate < 32; inGate++) {
                int[] vals = NConfig.gateToXY(inGate);
                int outGate = possibilities[i].performExperiment(vals[0], vals[1], vals[2], new ArrayList<>());
                gates[i][inGate] = outGate;
                inOut[inGate][outGate + 1]++;
            }
        }
        System.out.println("{");
        for (int[] f : inOut) {
            System.out.print("{");
            for (int i = 0; i < f.length; i++) {
                if (i < f.length - 1) {
                    System.out.print(f[i] + ",\t");
                } else {
                    System.out.print(f[i]);
                }
            }
            System.out.println("},");
        }
        System.out.println("}");
    }

    public static int generate(NConfig n, int amountOfSetTiles, int startTile, NConfig[] possibilities, int counter) {
        if (amountOfSetTiles == 4) {
            possibilities[counter++] = n.copy();
        }
        if (amountOfSetTiles < 4) {
            amountOfSetTiles++;
            for (int y = startTile / 6; y < 6; y++) {
                for (int x = startTile % 6; x < 6; x++) {
                    startTile++;
                    n.forceSetTile(x + 1, y + 1, 0b10);
                    counter = generate(n, amountOfSetTiles, startTile, possibilities, counter);
                    n.forceSetTile(x + 1, y + 1, 0b01);
                }
            }
        }
        return counter;
    }
}
