package Utils;

import Configs.NConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Linus Hein on 01.08.2016.
 */
public class LookupTable {
    private static double log2 = Math.log(2);
    public static void main(String[] args) {
        /*long[] bits1 = put5Bits();
        Set<Long> allBS = new HashSet<>();
        for (long l : bits1) {
            allBS.add(new Long(l));
        }
        long[] bits2 = new long[376992];
        bits2[0] = 0b1011110;
        long number = 68_719_476_735;
        for (int i = 1; i < 376992; i++) {
            long tmp = bits2[i-1];
            System.out.println(Long.toBinaryString(tmp));
            for(int j=1;j<35;j++){
                if(((tmp>>j)&0b11) == 0b01){
                    j++;
                    bits2[i]= (((long)(0xFFFF_FFFF)<<j)&tmp) | ((tmp&1)<<j) | ((((long)(0x7FFF_FFFF)>>(36-j))&tmp)>>1);
                    j--;
                    break;
                }
            }
            if(bits2[i]==0){
                bits2[i]= ((tmp&1)<<35) | (tmp>>1);
            }
        }
        for(long l : bits2){
            if(!allBS.contains(new Long(l)))
                System.out.println(Long.toBinaryString(l));
            else
                allBS.remove(new Long(l));
        }*/
        Long[][][] lookUpTable = generateLookUpTable();
        long startTime = System.currentTimeMillis();
        int[][] experiments = new int[][]{
                /*{30, 1},
                {5, 9},
                {17, 14},
                {29, -1},
                {15, 24},
                {12, -1},
                {11, -1},
                {16, 7},
                {27, -1},
                {3, 3}};*/
                {0, 23},
                {1, 28},
                {7, 8},
                {20, -1},
                {19, 25},
                {10, 16},
                {2, 31},
                {4, 5},
                {12, -1},
                {29, -1}};
        Long[] C = lookUpSolutions(experiments, lookUpTable);
        System.out.println(System.currentTimeMillis() - startTime);
        for (Long aC : C)
            if (aC != 0) {
                long a = aC;
                for (int i = 0; i < 36; i++) {
                    System.out.print(((a & 0x8_0000_0000L) >> 35) + " ");
                    a <<= 1;
                    if (i % 6 == 5)
                        System.out.println();
                }
                System.out.println();
            }
    }

    public static Long[] lookUpSolutions(int[][] experiments, Long[][][] lookUpTable) {
        Arrays.sort(experiments, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return lookUpTable[o1[0]][o1[1]+1].length-lookUpTable[o2[0]][o2[1]+1].length;
            }
        });
        Long[] A = lookUpTable[experiments[0][0]][experiments[0][1] + 1];
        for (int i = 1; i < experiments.length; i++) {
            A = intersectArrays(A, lookUpTable[experiments[i][0]][experiments[i][1] + 1]);
        }
        return A;
    }

    private static Long[] intersectArrays(Long[] A, Long[] B) {
        ArrayList<Long> C = new ArrayList<>();
        if (A.length + B.length < Math.min(A.length, B.length) * Math.log(Math.max(A.length, B.length))/log2) {
            int a = 0, b = 0;
            while (a < A.length && b < B.length) {
                if (A[a].longValue() == B[b].longValue()) {
                    C.add(A[a]);
                    a++;
                    b++;
                } else if (A[a].longValue() < B[b].longValue())
                    a++;
                else
                    b++;
            }
        } else {
            if (A.length > B.length) {
                Long[] T = A;
                A = B;
                B = T;
            }
            for (Long a : A) {
                int l = 0, r = B.length - 1,tmp = (l + r) / 2;
                do {
                    if (a.longValue() == B[tmp].longValue()) {
                        C.add(a);
                        break;
                    }
                    if (a.longValue() < B[tmp].longValue())
                        r = tmp;
                    else
                        l = tmp;
                    tmp = (l+r)/2;
                } while (l != tmp && r != tmp);
            }
        }
        return C.toArray(new Long[C.size()]);
    }

    public static Long[][][] generateLookUpTable() {
        NConfig n = new NConfig();
        for (int i = 1; i < 7; i++) {
            for (int j = 1; j < 7; j++) {
                n.setTile(i, j, 0b01);
            }
        }
        NConfig[] array = new NConfig[376992];
        ArrayList<Long>[][] lookupTable = new ArrayList[32][33];
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 33; j++) {
                lookupTable[i][j] = new ArrayList<>();
            }
        }
        GenerateAllPossibilities.generate(n, -1, 0, array, 0);
        for (int i = 0; i < 376992; i++) {
            for (int inGate = 0; inGate < 32; inGate++) {
                int[] vals = NConfig.gateToXY(inGate);
                int outGate = array[i].performExperiment(vals[0], vals[1], vals[2], new ArrayList<>());
                lookupTable[inGate][outGate + 1].add(convertNConfigToBitString(array[i]));
            }
        }
        for (ArrayList[] a : lookupTable) {
            for (ArrayList b : a) {
                b.sort((o1, o2) -> ((Long) (o1)).compareTo((Long) o2));
            }
        }
        Long[][][] finalTable = new Long[32][33][];
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 33; j++) {
                Long[] tmp = new Long[lookupTable[i][j].size()];
                for (int k = 0; k < tmp.length; k++) {
                    tmp[k] = lookupTable[i][j].get(k);
                }
                finalTable[i][j] = tmp;
            }
        }
        return finalTable;
    }

    public static long convertNConfigToBitString(NConfig c) {
        long output = 0;
        for (int y = 1; y < 7; y++) {
            for (int x = 1; x < 7; x++) {
                if (c.isTile(x, y, 0b10)) {
                    output |= 1;
                }
                output = output << 1;
            }
        }
        return output >> 1;
    }
}
