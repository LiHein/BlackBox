package Utils;

import Configs.Configuration_OLD;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Obsolete.
 * Created by linus on 12.03.2016.
 */
public class Solver {
    public static void main(String[] args) {
        for (int abc = 0; abc < 100; abc++) {
            long start = System.nanoTime();
            Tile[][] o = Configuration_OLD.empty();
            Configuration_OLD c = new Configuration_OLD(o);
            List<Integer[]> experiments = new ArrayList<>();
            experiments.add(new Integer[]{28, 27});
            experiments.add(new Integer[]{14, 16});
            experiments.add(new Integer[]{19, 21});
            experiments.add(new Integer[]{5, 11});
            experiments.add(new Integer[]{4, -1});
            experiments.add(new Integer[]{3, -1});
            experiments.add(new Integer[]{18, -1});

            List<ArrayList<Configuration_OLD>> unmerged = new ArrayList<>();
            for (Integer[] e : experiments) {
                ArrayList<Configuration_OLD> l = new ArrayList<>();
                c.deriveConfiguration(e[0], e[1], l);
                unmerged.add(l);
            }
            List<Configuration_OLD> results = new ArrayList<>();
            results.addAll(unmerged.get(0));
            for (int a = 1; a < unmerged.size(); a++) {
                List<Configuration_OLD> otherList = unmerged.get(a), newList = new ArrayList<>();
                for (Configuration_OLD result : results) {
                    for (Configuration_OLD anOtherList : otherList) {
                        Configuration_OLD merged = Configuration_OLD.merge(result, anOtherList);
                        if (merged != null) {
                            newList.add(merged);
                        }
                    }
                }
                Set<Configuration_OLD> s = new CopyOnWriteArraySet<>();
                s.addAll(newList);
                results = new ArrayList<>();
                results.addAll(s);
            }
            results.forEach(System.out::println);
            System.out.println((System.nanoTime() - start) / 1000000);
        }
    }
}
