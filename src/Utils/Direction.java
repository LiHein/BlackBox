package Utils;

/**
 * This enum gives directions names.
 * Created by linus on 22.03.2016.
 */
public enum Direction {
    Right,
    Up,
    Left,
    Down
}
