package Utils;

import Configs.NConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * This Class can generate test cases in accordance to the description in the pdf.
 * Created by linus on 28.03.2016.
 */
public class GenerateExperiment {
    public static int seed = 1;
    public static Random r = new Random(seed);

    public static void resetRnd(){
        r=new Random(seed);
    }

    public static void rndSeed(){
        Random rnd = new Random();
        seed = rnd.nextInt();
    }

    public static void main(String[] args) {
        System.out.println(generateInput());
    }

    public static String generateInput() {
        System.out.print("50\n");
        String output = "50\n";
        for (int h = 0; h < 50; h++) {
            NConfig c = new NConfig();
            Random r = new Random();
            for (int i = 0; i < 5; i++) {
                int x = r.nextInt(6) + 1, y = r.nextInt(6) + 1;
                if (c.isTile(x, y, 0B10))
                    i--;
                c.setTile(x, y, 0B10);
            }
            //System.out.println(c.toString());
            Set<Integer> inGates = new CopyOnWriteArraySet<>();
            System.out.print("\n10\n");
            output += "\n10\n";
            for (int i = 0; i < 10; i++) {
                int inGate = r.nextInt(32);
                if (!inGates.contains(inGate)) {
                    int[] pos = NConfig.gateToXY(inGate);
                    int outGate = c.performExperiment(pos[0], pos[1], pos[2], new ArrayList<>());
                    System.out.println(inGate + " " + outGate);
                    output += inGate + " " + outGate + "\n";
                    inGates.add(inGate);
                    if (outGate != 0)
                        inGates.add(outGate);
                } else
                    i--;
            }
        }
        return output;
    }

    /**
     * Generates an array of possible inputs for the Blackbox-problem.
     * array[a][b][c]: a = ExperimentNumber ; b = LaserNumber; c = 0 = inGate ; c = 1 = outGate
     *
     * @param amountOfExperiments Amount of Experiments to be generated
     * @param amountOfLasers      Amount of lasers per experiment
     * @return array of experiments
     */
    public static int[][][] generateExperimentsArr(int amountOfExperiments, int amountOfLasers) {
        int[][][] output = new int[amountOfExperiments][amountOfLasers][2];
        for (int e = 0; e < amountOfExperiments; e++) {
            NConfig c = new NConfig();
            for (int i = 0; i < 5; i++) {
                int x = r.nextInt(6) + 1, y = r.nextInt(6) + 1;
                if (c.isTile(x, y, 0B10))
                    i--;
                c.setTile(x, y, 0B10);
            }
            Set<Integer> inGates = new CopyOnWriteArraySet<>();
            for (int i = 0; i < amountOfLasers; i++) {
                int inGate = r.nextInt(32);
                if (true|!inGates.contains(inGate)) {
                    int[] pos = NConfig.gateToXY(inGate);
                    int outGate = c.performExperiment(pos[0], pos[1], pos[2], new ArrayList<>());
                    inGates.add(inGate);
                    if (outGate != 0)inGates.contains(null);
                        //inGates.add(outGate);
                    output[e][i][0] = inGate;
                    output[e][i][1] = outGate;
                } else {
                    i--;
                }
            }
        }
        return output;
    }

    public static List<List<Integer[]>> generateExperimentsList(int amountOfExperiments, int amountOfLasers) {
        List<List<Integer[]>> output = new ArrayList<>(amountOfExperiments);
        for (int e = 0; e < amountOfExperiments; e++) {
            output.add(new ArrayList<>(amountOfLasers));
            NConfig c = new NConfig();
            for (int i = 0; i < 5; i++) {
                int x = r.nextInt(6) + 1, y = r.nextInt(6) + 1;
                if (c.isTile(x, y, 0B10))
                    i--;
                c.setTile(x, y, 0B10);
            }
            Set<Integer> inGates = new CopyOnWriteArraySet<>();
            for (int i = 0; i < amountOfLasers; i++) {
                int inGate = r.nextInt(32);
                //output.get(e).add(new ArrayList<>(amountOfLasers));
                if (!inGates.contains(inGate)) {
                    int[] pos = NConfig.gateToXY(inGate);
                    int outGate = c.performExperiment(pos[0], pos[1], pos[2], new ArrayList<>());
                    inGates.add(inGate);
                    if (outGate != 0)inGates.contains(null);
                        //inGates.add(outGate);
                    output.get(e).add(new Integer[]{inGate, outGate});

                } else {
                    i--;
                }
            }
        }
        return output;
    }
}
