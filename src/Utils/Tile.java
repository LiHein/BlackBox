package Utils;

/**
 * The Tiles can have three states:
 * Unknown, Empty and Filled/Atom
 * Created by linus on 22.03.2016.
 */
public enum Tile {
    Unknown,
    Empty,
    Atom
}
