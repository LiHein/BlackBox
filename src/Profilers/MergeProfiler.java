package Profilers;

import Configs.Configuration;
import Utils.GenerateExperiment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Linus Hein on 03.08.2016.
 */
public class MergeProfiler {
    public static void main(String[] args) {
        GenerateExperiment.rndSeed();
        GenerateExperiment.resetRnd();
        profileMerge(10000,10);
    }

    public static void profileMerge(int experimentNumber, int lasersPerExperiment) {
        int filterStrength = experimentNumber / 20;
        List<List<Integer[]>> exp = GenerateExperiment.generateExperimentsList(experimentNumber, lasersPerExperiment);
        long times[] = new long[experimentNumber];
        for (int i = 0; i < experimentNumber; i++) {
            long startTime = System.nanoTime();
            ArrayList<List<Integer[]>> tmp = new ArrayList<>();
            tmp.add(exp.get(i));
            Configuration.solveMerge(tmp);
            times[i] = System.nanoTime() - startTime;
        }
        long totalTime = 0;
        double geoMetricAvg = 0;
        for (long time : times) {
            totalTime += time;
            geoMetricAvg += Math.log(time);
        }
        geoMetricAvg = Math.exp(geoMetricAvg / experimentNumber);
        System.out.println(experimentNumber + " Experiments Merge:");
        System.out.println("Avg Time:\t\t" + totalTime / (1000000.f * experimentNumber));
        System.out.println("Total Time:\t\t" + totalTime / 1000000.f);
        System.out.println("First Time:\t\t" + times[0] / 1000000.f);
        System.out.println("Geo. Avg.Time:\t" + geoMetricAvg / 1000000.f);

        for (int i = 0; i < filterStrength; i++) {
            //sorting out the n best and worst times
            int max = 0, min = 0;
            for (int j = 0; j < experimentNumber; j++) {
                if ((times[j] < times[min] && times[j] != -1) || times[min] == -1)
                    min = j;
                if (times[j] > times[max])
                    max = j;
            }
            times[max] = times[min] = -1;
        }
        totalTime = 0;
        geoMetricAvg = 0;
        for (long time : times) {
            if (time != -1) {
                totalTime += time;
                geoMetricAvg += Math.log(time);
            }
        }
        geoMetricAvg = Math.exp(geoMetricAvg / (experimentNumber - 2.d * filterStrength));
        System.out.println("without the " + filterStrength + " best and worst runs");
        System.out.println("Avg Time:\t\t" + totalTime / (1000000.f * (experimentNumber - 2 * filterStrength)));
        System.out.println("Total Time:\t\t" + totalTime / 1000000.f);
        System.out.println("Geo. Avg.Time:\t" + geoMetricAvg / 1000000.f);
    }
}
