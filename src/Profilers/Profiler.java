package Profilers;

import Utils.GenerateExperiment;

import java.util.Random;

/**
 * Created by Linus Hein on 09.08.2016.
 */
public class Profiler {
    public static void main(String[] args) {
        LookUpProfiler.setup();
        for (int i = 1; i <= 32; i++) {
            GenerateExperiment.rndSeed();
            GenerateExperiment.resetRnd();

            System.out.println("Exp-num "+i+"\tSeed " + GenerateExperiment.seed + ":");

            LookUpProfiler.profileLookup(10000,i);
            System.out.println();

            GenerateExperiment.resetRnd();
            MergeProfiler.profileMerge(10000,i);
            System.out.println();

            GenerateExperiment.resetRnd();
            RebaseProfiler.profileRebase(10000,i);
            System.out.println();
        }
    }
}
